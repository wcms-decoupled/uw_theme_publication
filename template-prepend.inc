<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" data-ng-app="magApp" data-ng-cloak data-ng-controller="metaController" class="no-js lt-ie9 lt-ie8 lt-ie7" xmlns:og="http://opengraphprotocol.org/schema/" data-ng-init="pubType='<?php print $publication_theme; ?>'"><![endif]-->
<!--[if IE 7]><html lang="en" data-ng-app="magApp" data-ng-cloak data-ng-controller="metaController" class="no-js lt-ie9 lt-ie8" xmlns:og="http://opengraphprotocol.org/schema/" data-ng-init="pubType='<?php print $publication_theme; ?>'"><![endif]-->
<!--[if IE 8]><html lang="en" data-ng-app="magApp" data-ng-cloak data-ng-controller="metaController" class="no-js lt-ie9" xmlns:og="http://opengraphprotocol.org/schema/" data-ng-init="pubType='<?php print $publication_theme; ?>'"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" data-ng-app="magApp" data-ng-controller="MetaController" data-ng-cloak class="no-js" xmlns:og="http://opengraphprotocol.org/schema/" data-ng-init="pubType='<?php print $publication_theme; ?>'"><!--<![endif]-->
<head prefix="og: http://ogp.me/ns#" data-ng-init="publication_colorBar='<?php print $publication_colorBar; ?>'">
  <title>University of Waterloo</title>
  <script>
    document.title = "{{ metaData().metatags['title'] }}";
  </script>
  <meta name="fragment" content="!">
  <base href="<?php print $base_path; ?>">
  <meta name="robots" content="{{ metaData().metatags.robots }}">
  <meta name="keyword" content="{{ metaData().metatags.keywords }}">
  <meta name="news_keywords" content="{{ metaData().metatags.news_keywords }}">
  <meta name="description" content="{{metaData().metatags.description }}">
  <meta name="abstract" content="{{ metaData().metatags.abstract }}">
  <meta name="author" content="{{ metaData().metatags.author }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
  <!--og-Tags-->
  <meta property="og:title" content="{{ metaData().metatags['og:title'] }}">
  <meta property="og:type" content="{{ metaData().metatags['og:type'] }}">
  <meta property="og:locale" content="{{ metaData().metatags['og:locale'] }}">
  <meta property="og:locale:alternate" content="{{ metaData().metatags['og:locale:alternate'] }}">
  <meta property="og:site_name" content="{{ metaData().metatags['og:site_name'] }}">
  <meta property="og:description" content="{{ metaData().metatags['og:description'] }}">
  <meta property="og:determiner" content="{{ metaData().metatags['og:dterminer'] }}">
  <meta property="og:updated_time" content="{{ metaData().metatags['og:updated_time'] }}">
  <meta property="og:url" content="{{ metaData().metatags['og:url'] }}">
  <meta property="og:see_also" content="{{ metaData().metatags['og:see_also'] }}">
  <!-- og: geo location -->
  <meta property="og:street-address" content="{{ metaData().metatags['og:street-address'] }}">
  <meta property="og:locality" content="{{ metaData().metatags['og:locality'] }}">
  <meta property="og:region" content="{{ metaData().metatags['og:region'] }}">
  <meta property="og:postal-code" content="{{ metaData().metatags['postal-code'] }}">
  <meta property="og:country-name" content="{{ metaData().metatags['country-name'] }}">
  <meta property="og:latitude" content="{{ metaData().metatags['og:latitude'] }}">
  <meta property="og:longitude" content="{{ metaData().metatags['og:longitude'] }}">
  <!-- og:profile-->
  <meta property="profile:first_name" content="{{ metaData().metatags['first_name'] }}">
  <meta property="profile:last_name" content="{{ metaData().metatags['last_name'] }}">
  <meta property="profile:gender" content="{{ metaData().metatags['profile:gender'] }}">
  <meta property="profile:username" content="{{ metaData().metatags['profile:username'] }}">
  <meta property="og:email" content="{{ metaData().metatags['og:email'] }}">
  <meta property="og:phone_number" content="{{ metaData().metatags['og:phone_number'] }}">
  <meta property="og:fax_number" content="{{ metaData().metatags['fax_number'] }}">
  <!-- og:article-->
  <meta property="article:published_time" content="{{ metaData().metatags['article:published_time'] }}">
  <meta property="article:modified_time" content="{{ metaData().metatags['article:modified_time'] }}">
  <meta property="article:publisher" content="{{ metaData().metatags['article:publisher'] }}">
  <meta property="article:author" content="{{ metaData().metatags['article:author'] }}">
  <meta property="article:section" content="{{ metaData().metatags['article:section'] }}">
  <meta property="article:tag" content="{{ metaData().metatags['article:tag'] }}">
  <!--og:image array-->
  <meta property="og:image" content="{{ metaData().metatags['og:image'] }}">
  <meta property="og:image:secure_url" content="{{ metaData().metatags['og:image'] }}">
  <meta property="og:image:type" content="{{ metaData().metatags['og:image:type'] }}">
  <meta property="og:image:width" content="{{ metaData().metatags['og:image:width'] }}">
  <meta property="og:image:height" content="{{ metaData().metatags['og:image:height'] }}">
  <!--og:video-->
  <meta property="og:video" content="{{ metaData().metatags['og:image:height'] }}">
  <meta property="og:video:secure_url" content="{{ metaData().metatags['og:video:secure_url'] }}">
  <meta property="og:video:type" content="{{ metaData().metatags['og:video:type'] }}">
  <meta property="og:video:width" content="{{ metaData().metatags['og:video:width'] }}">
  <meta property="og:video:height" content="{{ metaData().metatags['og:video:height'] }}">
  <meta property="og:video:release_date" content="{{ metaData().metatags['og:video:release_date'] }}">
  <meta name="video:director" content="{{ metaData().metatags['video:director'] }}">
  <meta name="video:actor" content="{{ metaData().metatags['video:actor'] }}">
  <meta name="video:actor:role" content="{{ metaData().metatags['video:actor:role'] }}">
  <meta name="video:actor:role" content="{{ metaData().metatags['video:actor:role'] }}">
  <meta name="video:duration" content="{{ metaData().metatags['video:duration'] }}">
  <meta name="video:writer" content="{{ metaData().metatags['video:writer'] }}">
  <meta name="video:tag" content="{{ metaData().metatags['video:tag'] }}">
  <!--og:audio-->
  <meta property="og:audio" content="{{ metaData().metatags['og:audio'] }}">
  <meta property="og:audio:secure_url" content="{{ metaData().metatags['og:audio:secure_url'] }}">
  <meta property="og:audio:type" content="{{ metaData().metatags['og:audio:type'] }}">
  <!--twitter-card-->
  <meta name="twitter:title" content="{{ metaData().metatags['twitter:title'] }}">
  <meta name="twitter:description" content="{{ metaData().metatags['twitter:description'] }}">
  <meta name="twitter:url" content="{{ metaData().metatags['twitter:url'] }}">
  <meta name="twitter:card" content="{{ metaData().metatags['twitter:card'] }}">
  <meta name="twitter:site" content="{{ metaData().settings.twitter }}">
  <meta name="twitter:image:src" content="{{ metaData().metatags['twitter:image:src'] }}">
  <meta name="twitter:image:height" content="{{ metaData().metatags['twitter:image:height'] }}">
  <meta name="twitter:image:width" content="{{ metaData().metatags['twitter:image:width'] }}">
  <meta name="twitter:creator" content="{{ metaData().metatags['twitter:creator'] }}">
  <meta name="twitter:creator:id" content="{{ metaData().metatags['twitter:creator:id'] }}">
  <!--facebook og-->
  <script src="apps/uw_publication/js/jquery/dist/jquery.min.js"></script>
  <script src="apps/uw_publication/js/angular.1.4.5/angular/angular.min.js"></script>
  <script src="apps/uw_publication/js/angular.1.4.5/angular-animate/angular-animate.min.js"></script>
  <script src="apps/uw_publication/js/angularjs-socialshare/dist/angular-socialshare.min.js"></script>
  <script src="apps/uw_publication/js/foundation/js/foundation.min.js"></script>
  <script src="apps/uw_publication/js/angular-foundation/mm-foundation.min.js"></script>
  <script src="apps/uw_publication/js/foundation/js/foundation/foundation.interchange.js"></script>
  <script src="apps/uw_publication/js/angular-ui-router/release/angular-ui-router.min.js"></script>
  <script src="apps/uw_publication/js/angular.1.4.5/angular-sanitize/angular-sanitize.js"></script>
  <script src="apps/uw_publication/js/OwlCarousel/owl-carousel/owl.carousel.js"></script>
  <script src="apps/uw_publication/js/fdsu/d3.min.js"></script>
  <script src="apps/uw_publication/js/fdsu/d3_tip.js"></script>
  <script src="apps/uw_publication/js/fdsu/ckeditor_socialmedia.js"></script>
  <script src="apps/uw_publication/js/fdsu/mathjax.js"></script>
  <script src="apps/uw_publication/js/fdsu/uw_ct_embedded_facts_and_figures.js"></script>
  <script src="apps/uw_publication/js/fdsu/uw_ct_image_gallery.js"></script>
  <script src="apps/uw_publication/js/main.js?v=1"></script>
  <script src="apps/uw_publication/js/app.js?v=1"></script>
  <script src="apps/uw_publication/js/angulartics/dist/angulartics.min.js"></script>
  <script src="apps/uw_publication/js/a-gtm-uw/dist/a-gtm-uw.min.js"></script>
  <?php
  if (!empty($add_js)) {
    foreach ($add_js as $this_js) {
      echo '<script src="' . htmlentities($this_js, ENT_QUOTES) . '"></script>';
    }
  }
  ?>
  <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="apps/uw_publication/favicon.ico"/>
  <link rel="canonical" href="{{ metaData().metatags.canonical }}"/>
  <link data-ng-if="pubType === 'publication'" rel="stylesheet" href="apps/uw_publication/stylesheets/app_pub.css" />
  <link data-ng-if="pubType === 'report'" rel="stylesheet" href="apps/uw_publication/stylesheets/app_report.css" />
  <link data-ng-if="pubType === 'magazine'" rel="stylesheet" href="apps/uw_publication/stylesheets/app_mag.css" />
  <link data-ng-if="pubType === 'sotu3'" rel="stylesheet" href="apps/uw_publication/stylesheets/app_sotu3.css" />
  <?php
  if (!empty($add_css)) {
    foreach ($add_css as $this_css) {
      echo '<link rel="stylesheet" href="' . htmlentities($this_css, ENT_QUOTES) . '" />';
    }
  }
  ?>
  <?php print $ga; ?>
  <?php if (isset($gtm)) { print '<script>' . $gtm['script'] . '</script>'; } ?>
</head>
<body data-ng-class="bodyClass" class="{{pubType}} {{publication_colorBar}}">
  <?php if (isset($gtm)) { print $gtm['noscript']; } ?>
  <noscript>
    <style>
      [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: block !important;
      }
      .error_message {
        background-color: #C00;
        padding: 1rem;
        color:#fff;
        font-weight:bold;
        text-align: center;
        position: fixed;
        width: 100%;
        z-index:12;
      }
    </style>
    <div class="error_message">
      We are sorry, but this site requires JavaScript.
    </div>
  </noscript>
  <div class="breakpoint"></div>
  <div id="skip-link">
    <a href="#main" class="show-for-sr">Skip to main content</a>
  </div>

  <!-- If pub type is publication then show this header -->
  <header id="top" data-ng-if="pubType === 'publication' || pubType === 'magazine'" class="uwm-header-wrapper row full-width" data-scroll>
    <div class="uwm-header">
      <div class="uwm-header-bar-wrapper" data-ng-class="{menuAbsolute:!boolChangeClass}">
        <div class="uwm-header-bar row " data-ng-class="{'search-open':!searchToggle}">
          <div class="uwm-logo-header small-12 medium-6 medium-centered xlarge-3 xlarge-uncentered columns">
            <a href="https://uwaterloo.ca/"><span class="show-for-sr">University of Waterloo</span></a>
          </div>
          <h1 data-ng-cloak data-ng-if="showHeading === 'yes'" class="fade small-12  large-6 medium-centered collapse columns uwm-site-title showHeading-h1"><a href="<?php print $base_path; ?>"  target="_self" data-ng-bind-html="metaData().metatags['og:site_name']"></a></h1>
          <div data-ng-cloak data-ng-if="showHeading === 'no'" class="fade small-12  large-6 medium-centered collapse columns uwm-site-title showHeading"><a href="<?php print $base_path; ?>" target="_self" data-ng-bind-html="metaData().metatags['og:site_name']"></a></div>
          <div role="search" class="search-button-wrap">
            <a href="#global-search" class="search-button" data-ng-keypress="setPressedKey($event)" data-ng-click="searchToggle=!searchToggle" aria-expanded="false"><span data-ng-hide="searchToggle" class="icon icon-close fade"></span><span data-ng-show="searchToggle" class="icon icon-search fade"></span>search</a>
          </div>
          <div class="uwm-search-wrap">
            <div id="global-search" class="sotu-uw-global-search fade" data-ng-hide="searchToggle">
              <form class="sotu-uw-form" method="get" action="//uwaterloo.ca/search">
                <label id="uw-search-label" for="uw-search-term" class="show-for-sr">Search</label>
                <input id="uw-search-term" class="uw-search-term" placeholder="Search the Publication" type="text" size="31" name="q">
                <input type="hidden" name="q" value="site:https://uwaterloo.ca<?php print $base_path; ?>">
                <input type="hidden" name="client" value="default_frontend">
                <input type="hidden" name="proxystylesheet" value="default_frontend">
                <input type="hidden" name="site" value="default_collection">
              </form>
            </div>
          </div>
          <div class="navigation-button-wrap">
            <a class="navigation-button" data-ng-click="searchToggle=searchToggle" aria-label="Navigation" aria-expanded="false" href="#navigation">Menu<span></span></a>
          </div>
        </div>
        <div class="uwm-top-nav-wrapper" data-nav-visible="false">
          <nav id="navigation" class="uwm-top-nav" data-ng-controller="TopMenuController" aria-label="Main">
            <ul class="uwm-main-nav small-12 columns" data-ng-cloak>
              <li data-ng-class="{'active':link.aliasCheck === stateName}" data-ng-repeat="link in topMenu" data-ui-sref-active="active">
                <a class="uwm-nav-link" href="{{ link.alias }}" data-ng-bind-html="link.label"></a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="uw-site--cbar">
          <div class="uw-site--c1 uw-cbar"></div>
          <div class="uw-site--c2 uw-cbar"></div>
          <div class="uw-site--c3 uw-cbar"></div>
          <div class="uw-site--c4 uw-cbar"></div>
        </div>
      </div>
    </div>
  </header>

  <!-- If pub type is report then show this header -->
  <header data-ng-if="pubType === 'report'" data-ng-class="{scrollShow:boolChangeClass}" class="uwm-header-wrapper row full-width" data-scroll>
    <div class="uwm-header">
      <div class="row uwm-logo-header-wrapper">
        <div class="small-8 small-centered medium-6 medium-uncentered large-4 columns uwm-logo-header">
          <a href="https://uwaterloo.ca/"><span class="show-for-sr">University of Waterloo</span></a>
        </div>
      </div>
      <div class="uwm-header-bar-wrapper" data-ng-class="{menuAbsolute:!boolChangeClass}">
        <div class="uwm-header-bar row " data-ng-class="{'search-open':!searchToggle}">
          <h1 data-ng-cloak data-ng-if="showHeading === 'yes'" class="fade small-12 medium-8 medium-centered collapse columns uwm-site-title showHeading-h1"><a href="<?php print $base_path; ?>" data-ng-bind-html="metaData().metatags['og:site_name']"></a></h1>
          <div data-ng-cloak data-ng-if="showHeading === 'no'" class="fade small-12 medium-8 medium-centered collapse columns uwm-site-title showHeading"><a href="<?php print $base_path; ?>" data-ng-bind-html="metaData().metatags['og:site_name']"></a></div>
          <div role="search" class="search-button-wrap">
            <a href="#global-search" class="search-button" data-ng-keypress="setPressedKey($event)" data-ng-click="searchToggle=!searchToggle" aria-expanded="false"><span data-ng-hide="searchToggle" class="icon icon-close fade"></span><span data-ng-show="searchToggle" class="icon icon-search fade"></span>search</a>
          </div>
          <div class="uwm-search-wrap">
            <div class="sotu-uw-global-search fade" data-ng-hide="searchToggle">
              <form class="sotu-uw-form" method="get" action="//uwaterloo.ca/search">
                <label id="uw-search-label" for="uw-search-term" class="show-for-sr">Search</label>
                <input id="uw-search-term" class="uw-search-term" placeholder="Search the Publication" type="text" size="31" name="q">
                <input type="hidden" name="q" value="site:https://uwaterloo.ca<?php print $base_path; ?>">
                <input type="hidden" name="client" value="default_frontend">
                <input type="hidden" name="proxystylesheet" value="default_frontend">
                <input type="hidden" name="site" value="default_collection">
              </form>
            </div>
          </div>
          <div class="navigation-button-wrap">
            <a class="navigation-button" data-ng-click="searchToggle=searchToggle" aria-label="Navigation" aria-expanded="false" href="#navigation">Menu<span></span></a>
          </div>
        </div>
        <div class="uwm-top-nav-wrapper" data-nav-visible="false">
          <nav id="navigation" class="uwm-top-nav" data-ng-controller="TopMenuController" aria-label="Main">
            <ul class="uwm-main-nav small-12 columns" data-ng-cloak>
              <li data-ng-class="{'active':link.aliasCheck === stateName}" data-ng-repeat="link in topMenu" data-ui-sref-active="active">
                <a class="uwm-nav-link" href="{{ link.alias }}" data-ng-bind-html="link.label"></a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>

  <!-- If pub type is sotu3 then show this header -->
  <header data-ng-if="pubType === 'sotu3'" class="uwm-header-wrapper row full-width" data-scroll>
    <div class="uwm-header">
      <div class="uwm-header-bar-wrapper" data-ng-class="{menuAbsolute:!boolChangeClass}">
        <div class="uwm-header-bar row " data-ng-class="{'search-open':!searchToggle}">
          <div class="uwm-logo-header small-12 medium-6 medium-centered xlarge-3 xlarge-uncentered columns">
            <a href="https://uwaterloo.ca/"><span class="show-for-sr">University of Waterloo</span></a>
          </div>
          <h1 data-ng-cloak data-ng-if="showHeading === 'yes'" class="fade small-12 medium-6 medium-centered collapse columns uwm-site-title showHeading-h1"><a href="<?php print $base_path; ?>" data-ng-bind-html="metaData().metatags['og:site_name']"></a></h1>
          <div data-ng-cloak data-ng-if="showHeading === 'no'" class="fade small-12 medium-6 medium-centered collapse columns uwm-site-title showHeading"><a href="<?php print $base_path; ?>" data-ng-bind-html="metaData().metatags['og:site_name']"></a></div>
          <div role="search" class="search-button-wrap">
            <a href="#global-search" class="search-button" data-ng-keypress="setPressedKey($event)" data-ng-click="searchToggle=!searchToggle" aria-expanded="false"><span data-ng-hide="searchToggle" class="icon icon-close fade"></span><span data-ng-show="searchToggle" class="icon icon-search fade"></span>search</a>
          </div>
          <div class="uwm-search-wrap">
            <div class="sotu-uw-global-search fade" data-ng-hide="searchToggle">
              <form class="sotu-uw-form" method="get" action="//uwaterloo.ca/search">
                <label id="uw-search-label" for="uw-search-term" class="show-for-sr">Search</label>
                <input id="uw-search-term" class="uw-search-term" placeholder="Search the Publication" type="text" size="31" name="q">
                <input type="hidden" name="q" value="site:https://uwaterloo.ca<?php print $base_path; ?>">
                <input type="hidden" name="client" value="default_frontend">
                <input type="hidden" name="proxystylesheet" value="default_frontend">
                <input type="hidden" name="site" value="default_collection">
              </form>
            </div>
          </div>
          <div class="navigation-button-wrap">
            <a class="navigation-button" data-ng-click="searchToggle=searchToggle" aria-label="Navigation" aria-expanded="false" href="#navigation">Menu<span></span></a>
          </div>
        </div>
        <div class="uwm-top-nav-wrapper" data-nav-visible="false">
          <nav id="navigation" class="uwm-top-nav" data-ng-controller="TopMenuController" aria-label="Main">
            <ul class="uwm-main-nav small-12 columns" data-ng-cloak>
              <li data-ng-class="{'active':link.aliasCheck === stateName}" data-ng-repeat="link in topMenu" data-ui-sref-active="active">
                <a class="uwm-nav-link" href="{{ link.alias }}" data-ng-bind-html="link.label"></a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="uw-site--cbar">
          <div class="uw-site--c1 uw-cbar"></div>
          <div class="uw-site--c2 uw-cbar"></div>
          <div class="uw-site--c3 uw-cbar"></div>
          <div class="uw-site--c4 uw-cbar"></div>
        </div>
      </div>
    </div>
  </header>
  <!--/header-->

  <!--main-->
  <!-- If pub type is magazine then show -->
  <div data-ng-if="pubType === 'magazine'" id="main" role="main" class="uwm-main-content-wrapper">
