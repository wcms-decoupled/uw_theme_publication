;(function UWMMenuInit($, window, document) {
  'use strict';

  $(document).ready(handleDocumentReady);
    function handleDocumentReady() {
	  var openMenu = function() {
	    $('.uwm-main-nav').attr({
          'data-nav-visible': 'true'
        });
		$('.navigation-button[href="#"]').attr({
	      'aria-expanded': 'true'
		});
		// setTimeout(function() {
		//   $('[href="#navigation"]').text('\u00D7');
		// }, 200);
	  }
	  var closeMenu = function() {
	    $('.uwm-main-nav').attr({
		  'data-nav-visible': 'false'
		});
		$('.navigation-button[href="#"]').attr({
		  'aria-expanded': 'false'
		});
		// setTimeout(function() {
		//   $('[href="#navigation"]').text('\u2630');
		// }, 200);
	  }
	  $('.navigation-button[href="#"]').on('click', function(e) {
	    e.preventDefault();
	    $(this).attr('aria-expanded') == 'true' ? closeMenu() : openMenu();
	  });

	  // At end of navigation block, return focus to navigation menu button
      $('#navigation li:last-child a').on('keydown', function(e) {
	    if (e.keyCode === 9) {
	      if (!e.shiftKey) {
	        e.preventDefault();
	        $('[href="#"]').focus();
	      }
	    }
	  });

	  // At start of navigation block, refocus close button on SHIFT+TAB
      $('#navigation li:first-child a').on('keydown', function(e) {
	    if (e.keyCode === 9) {
	      if (e.shiftKey) {
	        e.preventDefault();
	        $('.navigation-button[href="#"]').focus();
	      }
	    }
	  });

	  // If the menu is visible, always TAB into it from the menu button
	  $('[aria-expanded]').on('keydown', function(e) {
	    if (e.keyCode === 9) {
	      if ($(this).attr('aria-expanded') == 'true') {
	        if (!e.shiftKey) {
	          e.preventDefault();
	          $('#navigation li:first-child a').focus();
	        } else {
	          if (e.shiftKey) {
	            e.preventDefault();
	            $('#main').focus();
	          }
	        }
	      }
	    }
	  });
	}
})(window.jQuery, window, window.document);