(function ($) {  

  uw_ct_image_gallery = {

    attach: function (context, settings) {

      // This is the image gallery content type.
      if ($(".field-name-field-uw-image-gallery-images > .field-items").length) {

        // Check if this is a thumbnail or carousel for the image gallery content type.
        if ($('.field-name-field-image-gallery-type > .field-items > .field-item').text() == "Carousel" || !($('.field-name-field-image-gallery-type > .field-items > .field-item').length)) {
          // This is a carousel.

          // Add owl classes to the image gallery.
          $(".field-name-field-uw-image-gallery-images > .field-items").addClass("owl-carousel owl-theme");
  
          // Add the actual owl carousel to the image gallery, with single image viewing, no auto playing and lazy loading.
          $(".field-name-field-uw-image-gallery-images > .field-items").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            itemsScaleUp:true,
            lazyLoad: true,
            autoHeight: true,
            autoWidth:true,
            rewindSpeed: 10
          });
  
          // Declare variables used for moving the title and captions into the image gallery.
          var title_caption = {};
          var counter = 0;
          var current_image;
  
          // Select all of the images in the gallery.
          $('.owl-item > .field-item img').each(function(index) {
            // Setup the array to be used to store the titles and captions
            title_caption[counter] = {};

            // Get all the titles and insert them into the title_captions array.
            // Check if there is a title and a caption, which is seperated by a comma(,).
            if($(this).attr('title').match(/Title:(.*),/)) {
              title = $(this).attr('title').match(/Title:(.*),/);
              title_caption[counter]['title'] = title[1];
            }
            // Check if there is only a title, there will be no comma(,).
            else if($(this).attr('title').match(/Title:(.*)/)) {
              title = $(this).attr('title').match(/Title:(.*)/);
              title_caption[counter]['title'] = title[1];
            }
            // There is no title.
            else {
              title_caption[counter]['title'] = '';
            }

            // Get all the captions and place them in the title_captions array.
            // Check if there is a caption.
            if($(this).attr('title').match(/Caption:(.*)/)) {
              caption = $(this).attr('title').match(/Caption:(.*)/);
              title_caption[counter]['caption'] = caption[1];
            }
            // If there is no caption, put in a blank.
            else {
              title_caption[counter]['caption'] = '';
            }
  
            counter++;
          });

          if (!($('.owl-item > .field-item > .image-gallery-title-caption').length)) {
            // Place the title and caption before the image so that it can be used in the gallery.
            for ( var i = 0; i < counter; i++ ) {
              $($('.owl-item > .field-item').get(i)).prepend('<div class="image-gallery-title-caption"><span class="image-gallery-title-text">' + title_caption[i]['title'] + '</span><br /><span class="image-gallery-caption-text">' + title_caption[i]['caption'] + '</span></div>');
            }
          }
        }
        else {
          // This is the thumbnail for image gallery content type, add the css for the thumbnails.
          $('.field-name-field-uw-image-gallery-images > .field-items > .field-item').wrapAll('<div id="image-gallery-thumbnail">');
        }
      }

      // Setup embedded image gallery when using thumbnails.
      if ($("#image-gallery-thumbnail").length) {
        // Add a div after the images to store the captions, then hide them.
        // This will be used in the colorbox caption.
        $('<div id="image-gallery-captions-wrapper"></div>').insertAfter("#image-gallery-carousel");
        $('.image-gallery-caption').each(function(index) {
          $(this).appendTo('#image-gallery-captions-wrapper');
          $(this).css('display', 'none');
        });
      }

      // Setup embedded image gallery when using carousel.
      if ($("#image-gallery-carousel").length) {
        // Declare variables used for moving the title and captions into the image gallery.
        var title_caption = {};
        var counter = 0;
        var current_caption;


        // Select all of the images in the gallery.
        $('#image-gallery-carousel img').each(function(index) {
          // Setup the array to be used to store the titles and captions
          title_caption[counter] = {};

          // Store the title which is an attribute of the img tag.
          title_caption[counter]['title'] = $(this).attr('title');
          current_caption = $('.image-gallery-caption').get(index);
          title_caption[counter]['caption'] = $(current_caption).text();
          counter++;
        });

        // Add a div after the images to store the captions, then hide them.
        // This will be used in the colorbox caption.
        if (!($('#image-gallery-captions-wrapper').length)) {
          $('<div id="image-gallery-captions-wrapper"></div>').insertAfter("#image-gallery-carousel");
          $('.image-gallery-caption').each(function(index) {
            $(this).appendTo('#image-gallery-captions-wrapper');
            $(this).css('display', 'none');
          });
        }

        $("#image-gallery-carousel").owlCarousel({
          navigation : true, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          itemsScaleUp:true,
          lazyLoad: true,
          autoHeight: true,
          autoWidth:true,
          rewindSpeed: 10
        });

         // Place the title and caption before the image so that it can be used in the gallery.
        for ( var i = 0; i < counter; i++ ) {
          $($('.owl-item > .item').get(i)).prepend('<div class="image-gallery-title-caption"><span class="image-gallery-title-text">' + title_caption[i]['title'] + '</span><br /><span class="image-gallery-caption-text">' + title_caption[i]['caption'] + '</span></div>');
        }
      }

      // This is the onComplete function for colorbox.
      // Adding the caption to the colorbox.
      $(context).bind('cbox_complete', function () {
        // Find the current index of the image by using the cboxCurrent of colorbox.
        // cboxCurrent will have 1 of 6, 2 of 6, etc ..
        current_index = $(this).find('#cboxCurrent').html();
        current_index = current_index.substr(0,current_index.indexOf(' ')) - 1;

        current_title =  $(this).find('#cboxTitle');

        // Check if we are using the image gallery captions in the embedded format.
        if ($('.image-gallery-caption').get(current_index)) {
          // Get the current caption by using the current index that we found above.
          current_caption = $('.image-gallery-caption').get(current_index);
          current_caption = $(current_caption).html();

          // Set the title of the colorbox to the caption that we found above so
          // that it can be displayed.
          $(current_title).html(current_caption);
        }

        // If there is a caption, ensure that it shows.
        if ($(current_title).html() !== "") {
          $(current_title).show();
        }
      });
    }
  };
})(jQuery);
