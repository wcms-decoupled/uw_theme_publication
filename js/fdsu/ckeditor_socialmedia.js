(function ($) {  

      // Live stream
      if ($('div.cklivestream')) {
        $('div.cklivestream').hide().each(function() {
          lswidth = 500;
          lsheight = 307;
          lsfitwidth = $(this).parent().width();
          lsfitheight = Math.floor(lsfitwidth / lswidth * lsheight);
          lsurl = encodeURIComponent($(this).data('username'));
          //note: using HTTP for <a href> because Livestream has certificate problems.
          $(this).before('<iframe width="'+lsfitwidth+'" height="'+lsfitheight+'" src="//cdn.livestream.com/embed/'+lsurl+'?layout=4&width='+lsfitwidth+'&height='+lsfitheight+'&color=0x'+Drupal.settings.ckeditor_socialmedia.livestream_color+'&iconColor=0x'+Drupal.settings.ckeditor_socialmedia.livestream_icon_color+'&iconColorOver=0x'+Drupal.settings.ckeditor_socialmedia.livestream_icon_color_over+'" style="border:0;outline:0" frameborder="0" scrolling="no"></iframe><div><a href="http://www.livestream.com/'+lsurl+'">Watch video on Livestream</a></div>');
        });
      }

      // Facebook      
      if ( $('div.ckfacebook')) {
        $('div.ckfacebook').hide().each(function() {
          fbwidth = $(this).parent().width();
          fburl = encodeURIComponent('/'+$(this).data('username'));
          $(this).before('<iframe src="//www.facebook.com/plugins/likebox.php?href='+fburl+'&amp;width='+fbwidth+'&amp;height=350&amp;colorscheme='+Drupal.settings.ckeditor_socialmedia.facebook_colorscheme+'&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:'+fbwidth+'px; height:350px;" allowTransparency="true"></iframe>');
        });
      }

      // Old Twitter
      count = 0;
      $('div.cktwitter').not('[data-widget-id]').hide().each(function() {
        count++;
        //get variables
        twtype = $(this).data('type');
        twuser = $(this).data('username');
        twlist = $(this).data('listname');
        twsearch = $(this).data('search');
        twdisplay = $(this).data('displayname');
        //create div and widget (NOTE: 'subject' and 'title' have no effect on profile widgets)
        twdiv = "<div id=\"cktwitter"+count+"\" class=\"cktwitter_widget\"><script>new TWTR.Widget({  version: 2, type: '"+twtype+"',";
        if (twtype == 'search') {
          twdiv += " search: '"+twsearch+"',";
          twdiv += " title: 'Posts for',";
          twdiv += " subject: '"+twdisplay+"',";
        } else if (twtype == 'faves') {
          twdiv += " title: 'Favourites for',";
          twdiv += " subject: '"+twdisplay+"',";
        } else if (twtype == 'list') {
          twdiv += " title: 'List of',";
          twdiv += " subject: '"+twdisplay+"',";
        }
        twdiv += " rpp: 6, interval: 30000, width: 'auto', height: 257, footer: '', id: 'cktwitter"+count+"', theme: { shell: {background: '#"+Drupal.settings.ckeditor_socialmedia.twitter_shell_background+"', color: '#"+Drupal.settings.ckeditor_socialmedia.twitter_shell_foreground+"'}, tweets:{background: '#"+Drupal.settings.ckeditor_socialmedia.twitter_tweets_background+"', color: '#"+Drupal.settings.ckeditor_socialmedia.twitter_tweets_foreground+"', links: '#"+Drupal.settings.ckeditor_socialmedia.twitter_tweets_links+"'}}, features: {scrollbar: true, loop: false, live: true, behavior: 'all'}}).render()";
        if (twtype == 'profile' || twtype == 'faves') {
          twdiv += ".setUser('"+twuser+"')";
        } else if (twtype == 'list') {
          twdiv += ".setList('"+twuser+"', '"+twlist+"')";
        }
        twdiv += ".start();</script></div>";
        $(this).before(twdiv);
      });

      // New Twitter
      $('blockquote.twitter-tweet').hide();
      $('div.cktwitter[data-widget-id] a').hide().attr('width',$('div.cktwitter').parent().width());
      !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs"); //code from Twitter to activate widgets
      //if a new Twitter widget failed to load after 2 seconds, show the link (if Twitter does eventually load, it will hide the link itself)
      setTimeout(function() {
        //standard Twitter widgets
        $.each($('div.cktwitter[data-widget-id]'), function() {
          $iframe = $('iframe', this);
          if (!$iframe.length || $iframe.contents().find('body').html() == '') {
            $(this).closest('div').find('a').show();
          }
        });
        //embedded Tweets (Twitter removes the blockquote if it succeeds)
        $('blockquote.twitter-tweet').show();
      }, 2000);
}(jQuery));

