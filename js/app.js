// create the module and name it magApp
var magApp = angular.module('magApp', [ 'angulartics', 'angulartics.google.tagmanager', 'ui.router','mm.foundation','ngSanitize','mm.foundation.interchange', 'ngAnimate','720kb.socialshare'])
.constant('apiPath', 'api');
var appsPath = 'apps/uw_publication';
magApp.config(function($httpProvider){
    // Needed to connect to Drupal site.
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    // Set Accept header to hal+json to comply with Drupal GET
    $httpProvider.defaults.headers.common['Accept'] = 'application/hal+json';
});
magApp.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/404");
    // Strip trailing slashes so links with trailing slashes work.
    // From stackoverflow.com/questions/24420578/handling-trailing-slashes-in-angularui-router
    $urlRouterProvider.rule(function($injector, $location) {
      var path = $location.path();
      var hasTrailingSlash = path[path.length-1] === '/';
      if(hasTrailingSlash) {
        //if last charcter is a slash, return the same url without the slash  
        var newPath = path.substr(0, path.length - 1); 
        return newPath; 
      } 
    });
    // Now set up the states
    $stateProvider
        .state('page_404', {
            url: '/404',
            templateUrl: appsPath + '/templates/404.html',
            bodyClass: 'showmark uwm-404-page',
            showHeading: 'no'
        })
        .state('home', {
            url: '/',
            templateUrl: appsPath + '/templates/issue.html',
            controller : 'PublicationHomeController',
            bodyClass: 'no-showmark uwm-home-page',
            showHeading: 'yes'
        })
        .state('publication_archives', {
            url: '/archives',
            templateUrl: appsPath + '/templates/archives.html',
            controller : 'PublicationArchivesController',
            bodyClass: 'showmark uwm-archives-page',
            showHeading: 'no'
        })
        .state('unknown_bundle', {
            url: '/node/{nodeID:int}/draft',
            controller: 'DetermineBundleController'
        })

        .state('publication_types', {
            url: '/types/:termName',
            templateUrl: appsPath + '/templates/term_page.html',
            controller : 'PublicationTypesController',
            bodyClass: 'showmark uwm-term-page',
            showHeading: 'no'
        })
        .state('publication_themes', {
            url: '/themes/:termName',
            templateUrl: appsPath + '/templates/term_page.html',
            controller : 'PublicationThemesController',
            bodyClass: 'showmark uwm-term-page',
            showHeading: 'no'
        })
        .state('publication_issues', {
            url: '/issues/:termName',
            templateUrl: appsPath + '/templates/issue.html',
            controller : 'PublicationIssuesController',
            bodyClass: 'no-showmark uwm-issues-page',
            showHeading: 'yes'
        })
        .state('publication_promo_items', {
            url: '/testimonies',
            templateUrl: appsPath + '/templates/testimonies.html',
            controller : 'TestimoniesController',
            bodyClass: 'showmark uwm-testimonies-page'
        })  
        .state('publication_category', {
            url: '/categories/:termName',
            templateUrl: appsPath + '/templates/term_page.html',
            controller : 'PublicationCategoryController',
            bodyClass: 'showmark uwm-term-page',
            showHeading: 'no'
        })
        .state('two_level_path', {
            url: '/:issueName/:categoryName',
            bodyClass: 'showmark uwm-term-page',
            showHeading: 'no',
            controller: ['$state', '$stateParams', function($state,$stateParams){
              $state.go('publication_category', {'termName':$stateParams.categoryName });
            }]
        })
        .state('publication_article', {
            url: '/:issueName/:categoryName/:nodeName',
            templateUrl: appsPath + '/templates/publication_article.html',
            controller : 'PublicationArticleController',
            bodyClass: 'no-showmark uwm-pub-article-page',
            showHeading: 'no'
        })
        .state('web_pages', {
            url: '/{path:.*}',
            templateUrl: appsPath + '/templates/web_pages.html',
            controller : 'WebPagesController',
            bodyClass: 'showmark uwm-web-page-state',
            showHeading: 'no'
        });
    // HTML5 PUSH STATE
    $locationProvider.html5Mode(true).hashPrefix('!');
});
magApp.run(['$rootScope', '$state', '$stateParams','$log',function($rootScope, $state, $stateParams,$log){
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      
    var closeMenu = function() {
        $('.uwm-top-nav-wrapper').attr({
          'data-nav-visible': 'false'
        });
        $('[href="#navigation"]').attr({
          'aria-expanded': 'false'
        });
         $('.uwm-header-bar').removeClass('bt-menu-open');
         $('.uwm-header-bar').addClass('bt-menu-close');
         $('.uwm-header-wrapper').removeClass('menu-open');
         $('.uwm-left').removeClass('menu-open');
         
    };
    closeMenu();
  });
  $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
    $rootScope.bodyClass = toState.bodyClass;
    $rootScope.showHeading = toState.showHeading;
    setWindowHeight();
   $('div[class*="highlighted-fact-wrapper"]').owlCarousel();
    uwmScrollTop('body');

    if (toState.name === 'page_404') {
      $rootScope.stateName = $state.current.url;
      if ($rootScope.stateName) {
        unwantedCharacter = '/';
        while ($rootScope.stateName.charAt(0) == unwantedCharacter) $rootScope.stateName = $rootScope.stateName.substr(1);
      }
    } else if (toState.name === 'home') {

        $rootScope.stateName = $state.current.name;

    } else if (toState.name === 'publication_article') {

        $rootScope.stateName = $stateParams.nodeName;

    } else if (toState.name === 'web_pages') {

        $rootScope.stateName = $stateParams.url;

    } else if (toState.name === 'publication_archives') {

      $rootScope.stateName = $state.current.url;

      if ($rootScope.stateName) {
         unwantedCharacter = '/';
         while ($rootScope.stateName.charAt(0) == unwantedCharacter) $rootScope.stateName = $rootScope.stateName.substr(1);
     }
    } else if (toState.name === 'publication_issues'){
      
        $rootScope.stateName = $stateParams.termName;

    } else if (toState.name === 'publication_themes'){
        $rootScope.stateName = $stateParams.termName;

    } else if (toState.name === 'publication_category'){
       $rootScope.stateName = $stateParams.termName;

     } else if (toState.name === 'publication_types' ){
       $rootScope.stateName = $stateParams.termName;

     } else if (toState.name === 'two_level_path' ){
        $rootScope.stateName = $stateParams.termName;

    };
     
      // console.log( $rootScope.stateName);
      // console.log( $state);
  });

}]);
magApp.filter('boldFilter', function() {
     return function(text) {
     return String(text).replace(/\*(.+?)\*/g, '<span>$1</span>');
    };
});
magApp.filter('replaceHyphen', function() {
     return function(text) {
     return String(text).replace(/-/g,"_");
    };
});
magApp.filter('removeFilter', function() {
     return function(text) {
     return String(text).replace(/\*(.+?)\*/g, '$1');
    };
});
magApp.filter('matchLast', function() {
     return function(text) {
    
    return String(text).match(/([^\/]+$)/g);

    };
});

magApp.animation('.slide-toggle', ['$animateCss', function($animateCss) {
  var lastId = 0;
  var _cache = {};
  function getId(el) {
      var id = el[0].getAttribute("data-slide-toggle");
      if (!id) {
          id = ++lastId;
          el[0].setAttribute("data-slide-toggle", id);
      }
      return id;
  }
  function getState(id) {
      var state = _cache[id];
      if (!state) {
          state = {};
          _cache[id] = state;
      }
      return state;
  }
  function generateRunner(closing, state, animator, element, doneFn) {
      return function() {
          state.animating = true;
          state.animator = animator;
          state.doneFn = doneFn;
          animator.start().finally(function() {
              if (closing && state.doneFn === doneFn) {
                  element[0].style.height = '';
              }
              state.animating = false;
              state.animator = undefined;
              state.doneFn();
          });
      }
  }
  return {
      addClass: function(element, className, doneFn) {
          if (className == 'ng-hide') {
              var state = getState(getId(element));
              var height = (state.animating && state.height) ? 
                  state.height : element[0].offsetHeight;

              var animator = $animateCss(element, {
                  from: {height: height + 'px'},
                  to: {height: '0px'}
              });
              if (animator) {
                  if (state.animating) {
                      state.doneFn = 
                        generateRunner(true, 
                                       state, 
                                       animator, 
                                       element, 
                                       doneFn);
                      return state.animator.end();
                  }
                  else {
                      state.height = height;
                      return generateRunner(true, 
                                            state, 
                                            animator, 
                                            element, 
                                            doneFn)();
                  }
              }
          }
          doneFn();
      },
      removeClass: function(element, className, doneFn) {
          if (className == 'ng-hide') {
              var state = getState(getId(element));
              var height = (state.animating && state.height) ?  
                  state.height : element[0].offsetHeight;

              var animator = $animateCss(element, {
                  from: {height: '0px'},
                  to: {height: height + 'px'}
              });

              if (animator) {
                  if (state.animating) {
                      state.doneFn = generateRunner(false, 
                                                    state, 
                                                    animator, 
                                                    element, 
                                                    doneFn);
                      return state.animator.end();
                  }
                  else {
                      state.height = height;
                      return generateRunner(false, 
                                            state, 
                                            animator, 
                                            element, 
                                            doneFn)();
                  }
              }
          }
          doneFn();
      }
  };
}]);
magApp.directive("scroll", function ($window) {
  return function ($scope, element, attrs) {
    function getScrollOffsets(w) {
      // Use the specified window or the current window if no argument 
      w = w || window;
      // This works for all browsers except IE versions 8 and before
      if (w.pageXOffset != null) return {
          x: w.pageXOffset,
          y: w.pageYOffset
      };
      // For IE (or any browser) in Standards mode
      var d = w.document;
      if (document.compatMode == "CSS1Compat") {
        return {
          x: d.documentElement.scrollLeft,
          y: d.documentElement.scrollTop
        };
      }
      // For browsers in Quirks mode
      return {
        x: d.body.scrollLeft,
        y: d.body.scrollTop
      };
    }
    angular.element($window).bind("scroll", function () {
      var offset = getScrollOffsets($window);
      var windowSize = $(window).width();
        if (windowSize <= 1024) {
              logoHeight = 56;
        }
        else{
          
            logoHeight = 80;
        }
       if (offset.y >= logoHeight) {
         $scope.boolChangeClass = true;
       } else {
         $scope.boolChangeClass = false;
       }
      $scope.$apply();
    });
  };
});

magApp.controller('DetermineBundleController', ['$scope', '$state', '$stateParams', 'dftServiceDetermineBundle', 'apiPath', function($scope, $state, $stateParams, dftServiceDetermineBundle, apiPath) {
    dftServiceDetermineBundle.getNode($stateParams.nodeID, apiPath, function(data) {
        $scope.bundle = data['hal:determine_bundle'];
        switch($scope.bundle[0].bundle) {
            case 'uw_web_page':
                $state.go('web_pages', {path: 'node/' + $stateParams.nodeID + '/draft'});
                break;
            case 'publication_article':
                $state.go('publication_article', {issueName: "node", categoryName: $stateParams.nodeID, nodeName: 'draft'});
                break;
    }
    });
}]);
magApp.controller('PublicationHomeController', ['$filter','$scope', '$stateParams', 'dftServiceMetaData', 'dftServicePACurrentIssue', 'apiPath', function($filter,$scope,  $stateParams, dftServiceMetaData, dftServicePACurrentIssue, apiPath) {
    $scope.dataHasLoaded = false;
    dftServicePACurrentIssue.getNodes(apiPath, function(data) {
        $scope.nodes = data['hal:publication_articles_teaser'];
        if (Array.isArray($scope.nodes)){
           index = 0;  
           $scope.nodes.forEach(function (){
              if ($scope.nodes[index].sub_headline){
                 $scope.nodes[index].sub_headline = $filter('boldFilter')($scope.nodes[index].sub_headline);
              }
              $scope.nodes[index].headline = $filter('boldFilter')($scope.nodes[index].headline);
              $scope.nodes[index].teaser = $filter('boldFilter')($scope.nodes[index].teaser);
               index++;
            });
        }
        $scope.issue = $scope.nodes[0]._embedded['hal:publication_issues_basic'][0];
        $scope.issueName = $scope.issue.label;
        $scope.issueAlias = $scope.issue.alias;
      
        $scope.dataHasLoaded = true;
    });
    dftServiceMetaData.setMetaDataAPI(apiPath);
    $scope.metaData = dftServiceMetaData.getMetaData;
}]);
magApp.controller('PublicationIssuesController', ['$state','$filter','$scope', '$stateParams', 'dftServiceTermMetaData', 'dftServiceMetaData', 'dftServiceTermNode', 'dftServiceTerm', 'apiPath', function($state,$filter, $scope, $stateParams, dftServiceTermMetaData,  dftServiceMetaData, dftServiceTermNode, dftServiceTerm, apiPath) {
    $scope.dataHasLoaded = false;
    resource = 'publication_issues';
    termName = $stateParams.termName;
    filter = 'issue';

    dftServiceTermNode.getNode(filter, termName , apiPath, function(data) {
        $scope.nodes = data['hal:publication_articles_teaser'];
          if (Array.isArray($scope.nodes)){
             index = 0;  
             $scope.nodes.forEach(function (){
                if ($scope.nodes[index].sub_headline){
                    $scope.nodes[index].sub_headline = $filter('boldFilter')($scope.nodes[index].sub_headline);
                }
                $scope.nodes[index].headline = $filter('boldFilter')($scope.nodes[index].headline);
                 index++;
              });
          }
        machineName = $filter('replaceHyphen')($stateParams.termName);
        dftServiceTerm.getTerm(resource, machineName, apiPath, function(data) {
            $scope.issue = data['hal:' + resource][0];
            $scope.issueName = $scope.issue.label;
            $scope.termTitle = $scope.issue.name;
            $scope.issueAlias = $scope.issue.alias;
            dftServiceMetaData.setMetaTags(data['hal:' + resource][0].metatags);
        });
        $scope.dataHasLoaded = true;
    });

    // $state.go($state.current, $stateParams, {
    //   reload: true,
    //   inherit: false,
    //   notify: true
    // });
}]);
magApp.controller('MetaController', ['$scope', '$state','dftServiceMetaData', 'apiPath', function($scope, $state,  dftServiceMetaData, apiPath) {
     dftServiceMetaData.setMetaDataAPI(apiPath);
     $scope.metaData = dftServiceMetaData.getMetaData;
     $scope.caption = true;
     $scope.searchToggle = true;
     $scope.state= $state;
   
     $scope.setPressedKey = function(keyEvent) {
           var code = (keyEvent.keyCode || keyEvent.which);
             if(code == 13) {
              if(!$scope.searchToggle) {
                   event.preventDefault();
                   $scope.searchToggle = true;
                   return;
              }
              event.preventDefault();
              $scope.searchToggle = false;
            }
        }
}]);
magApp.controller('PublicationArchivesController', ['$scope', '$state', 'dftServicePublicationIssues', '$sce', 'apiPath', function($scope, $state, dftServicePublicationIssues, $sce, apiPath) {
    $scope.dataHasLoaded = false;
    dftServicePublicationIssues.getIssues(apiPath, function(data) {
        $scope.issues = data['hal:publication_issues'];
        
        if (Array.isArray($scope.issues)){
            index = 0;
            $scope.issues.forEach(function (issue){
                if (typeof $scope.issues[index].description !== "undefined" && $scope.issues[index].description !== null) {
                     $scope.issues[index].trustedDescription = $sce.trustAsHtml($scope.issues[index].description.replace(/\*(.+?)\*/g, '<span>$1</span>' ).toString());
                }
                index++;
            });
        }
     setWindowHeight();
     uwmScrollTop('body');
     $scope.dataHasLoaded = true;
    });
}]);

magApp.controller('PublicationCategoryController',['$filter','$scope','$stateParams', 'dftServiceTermMetaData', 'dftServiceMetaData', 'dftServiceTermNode', 'dftServiceTerm', 'apiPath', function($filter, $scope,  $stateParams, dftServiceTermMetaData,  dftServiceMetaData, dftServiceTermNode, dftServiceTerm, apiPath) {
      $scope.dataHasLoaded = false;
      resource = 'publication_categories';
      termName = $stateParams.termName;
      filter = 'categories';

      dftServiceTermNode.getNode(filter, termName , apiPath, function(data) {
          $scope.nodes = data['hal:publication_articles_teaser'];

            if (Array.isArray($scope.nodes)){
              index = 0;
             $scope.nodes.forEach(function (){
                if ($scope.nodes[index].sub_headline){
                   $scope.nodes[index].sub_headline = $filter('boldFilter')($scope.nodes[index].sub_headline);
                }
                $scope.nodes[index].headline = $filter('boldFilter')($scope.nodes[index].headline);
                $scope.nodes[index].teaser = $filter('boldFilter')($scope.nodes[index].teaser);
                $scope.nodes[index].issueLabel =  $scope.nodes[index]._embedded['hal:publication_issues_basic'][0].label;
                $scope.nodes[index].issueAlias =  $scope.nodes[index]._embedded['hal:publication_issues_basic'][0].alias;
                if($scope.nodes[index].in_current_issue){
                  $scope.nodes[index].issueAlias = $stateParams.home;
                }
                $scope.nodes[index].category = $scope.nodes[index]._embedded['hal:publication_categories_no_metadata'];
                if (Array.isArray($scope.nodes[index].category)){
                      index2 = 0;  
                          $scope.nodes[index].category.forEach(function (){
                          $scope.nodes[index].category[index2].name  =     $filter('boldFilter')($scope.nodes[index].category[index2].name);
                          $scope.nodes[index].category[index2].alias =     $filter('boldFilter')($scope.nodes[index].category[index2].alias);
                          index2++;
                      });
                  }
                 $scope.nodes[index].themes = $scope.nodes[index]._embedded['hal:publication_themes_no_metadata'];
                       if (Array.isArray($scope.nodes[index].themes)){
                          index3 = 0;  
                           $scope.nodes[index].themes.forEach(function (){
                            $scope.nodes[index].themes[index3].name  = $filter('boldFilter')($scope.nodes[index].themes[index3].name);
                            $scope.nodes[index].themes[index3].alias = $filter('boldFilter')($scope.nodes[index].themes[index3].alias);
                          index3++;
                      });
                   }
                 index++;
              });
             
          }
          $scope.dataHasLoaded = true;
           machineName = $filter('replaceHyphen')($stateParams.termName);
           dftServiceTerm.getTerm(resource, machineName, apiPath, function(data) {
               $scope.termTitle = data['hal:' + resource][0].name;
               dftServiceMetaData.setMetaTags(data['hal:' + resource][0].metatags);
           });
      });

}]);
magApp.controller('PublicationThemesController',['$filter','$scope', '$stateParams',  'dftServiceTermMetaData', 'dftServiceMetaData', 'dftServiceTermNode','dftServiceTerm', 'apiPath', function($filter,$scope, $stateParams, dftServiceTermMetaData, dftServiceMetaData, dftServiceTermNode, dftServiceTerm, apiPath) {
    resource = 'publication_themes';
    termName = $stateParams.termName;
    filter = 'themes';
    $scope.dataHasLoaded = false;
    dftServiceTermNode.getNode(filter, termName , apiPath, function(data) {
        $scope.nodes =  data['hal:publication_articles_teaser'];
           if (Array.isArray($scope.nodes)){
                index = 0;
               $scope.nodes.forEach(function (){
                 if ($scope.nodes[index].sub_headline){
                    $scope.nodes[index].sub_headline = $filter('boldFilter')($scope.nodes[index].sub_headline);
                  }
                  $scope.nodes[index].headline = $filter('boldFilter')($scope.nodes[index].headline);
                  $scope.nodes[index].teaser = $filter('boldFilter')($scope.nodes[index].teaser);
                  $scope.nodes[index].issueLabel =  $scope.nodes[index]._embedded['hal:publication_issues_basic'][0].label;
                  $scope.nodes[index].issueAlias =  $scope.nodes[index]._embedded['hal:publication_issues_basic'][0].alias;
                  if($scope.nodes[index].in_current_issue){
                    $scope.nodes[index].issueAlias = $stateParams.home;
                  }
                  $scope.nodes[index].category = $scope.nodes[index]._embedded['hal:publication_categories_no_metadata'];
                  if (Array.isArray($scope.nodes[index].category)){
                      index2 = 0;  
                           $scope.nodes[index].category.forEach(function (){
                            $scope.nodes[index].category[index2].name  =     $filter('boldFilter')($scope.nodes[index].category[index2].name);
                            $scope.nodes[index].category[index2].alias =     $filter('boldFilter')($scope.nodes[index].category[index2].alias);
                          index2++;
                      });
                  }
                  $scope.nodes[index].themes = $scope.nodes[index]._embedded['hal:publication_themes_no_metadata'];
                       if (Array.isArray($scope.nodes[index].themes)){
                          index3 = 0;  
                            $scope.nodes[index].themes.forEach(function (){
                            $scope.nodes[index].themes[index3].name  = $filter('boldFilter')($scope.nodes[index].themes[index3].name);
                            $scope.nodes[index].themes[index3].alias = $filter('boldFilter')($scope.nodes[index].themes[index3].alias);
                          index3++;
                      });
                    } 
                   $scope.nodes[index].types = $scope.nodes[index]._embedded['hal:publication_types_no_metadata'];
                    if (Array.isArray($scope.nodes[index].types)){
                        index4 = 0;  
                            $scope.nodes[index].types.forEach(function (){
                              $scope.nodes[index].types[index4].name  = $filter('boldFilter')($scope.nodes[index].types[index4].name);
                              $scope.nodes[index].types[index4].alias = $filter('boldFilter')($scope.nodes[index].types[index4].alias);
                            index4++;
                        });
                    }
                 index++;
                });
            }
        machineName = $filter('replaceHyphen')($stateParams.termName);
           dftServiceTerm.getTerm(resource, machineName, apiPath, function(data) {
               $scope.termTitle = data['hal:' + resource][0].name;
               dftServiceMetaData.setMetaTags(data['hal:' + resource][0].metatags);
           });
        $scope.dataHasLoaded = true;
    });
}]);
magApp.controller('publicationTypesController',['$scope', '$stateParams',  'dftServiceTermMetaData', 'dftServiceMetaData', 'dftServiceTermNode', 'dftServiceTerm' ,'apiPath', function($scope, $stateParams, dftServiceTermMetaData, dftServiceMetaData, dftServiceTermNode, dftServiceTerm, apiPath) {
    resource = 'publication_types';
    termName = $stateParams.termName;
    filter = 'contains';
    dftServiceTermNode.getNode(filter, termName , apiPath, function(data) {
        $scope.nodes =  data['hal:publication_articles_teaser'];
        $scope.types = $scope.nodes[0]._embedded['hal:publication_types_no_metadata'];
        $scope.themes = $scope.nodes[0]._embedded['hal:publication_themes_no_metadata'];
        $scope.termTitle =  $scope.nodes[0]._embedded['hal:publication_types_no_metadata'][0].name;
        dftServiceMetaData.setMetaTags($scope.types[0].metatags);
        machineName = $filter('replaceHyphen')($stateParams.termName);
           dftServiceTerm.getTerm(resource, machineName, apiPath, function(data) {
               $scope.termTitle = data['hal:' + resource][0].name;
               dftServiceMetaData.setMetaTags(data['hal:' + resource][0].metatags);
           });
    });
}]);
magApp.controller('PublicationArticleController',['$state','$timeout', '$rootScope','$filter','$scope', '$stateParams', 'dftServicePublicationArticles', 'dftServiceMetaData', '$sce', 'apiPath', function($timeout, $rootScope,$state,$filter,$scope, $stateParams, dftServicePublicationArticles, dftServiceMetaData, $sce, apiPath) {
    loadFromRevision = false;
    issueName = $stateParams.issueName;
    categoryName = $stateParams.categoryName;
    nodeName = $stateParams.nodeName;
    metaData = dftServiceMetaData.getMetaData();
    $scope.dataHasLoaded = false;
    dftServicePublicationArticles.getNode(issueName, categoryName, nodeName, apiPath, function(data) {
        $scope.node = data['hal:publication_articles'];
        dftServiceMetaData.setMetaTags($scope.node[0].metatags);
        $scope.sub_headline = $filter('boldFilter')($scope.node[0].sub_headline);
        $scope.headline = $filter('boldFilter')($scope.node[0].headline); 
        $scope.displayWideScreen = $scope.node[0].display_wide_screen;
        $scope.authors =  $scope.node[0].author;

         // Load the draft version if unpublished or viewing a draft revision.
        if (typeof $scope.node[0].current_status !== 'undefined') {
            $currentStatus = $scope.node[0].current_status;
            switch ($currentStatus) {
                case 'unpublished':
                    $scope.draftNode = true;
                case 'published':
            }
            if (nodeName == 'draft' && Array.isArray($scope.node[0].moderation_revision) && $scope.node[0].moderation_revision[0] !== null) {
                // Set the node to the revision.
                $scope.node = $scope.node[0].moderation_revision;
                $revisionState = $scope.node[0].revision_state;
                    switch ($revisionState) {
                        case 'archive':
                        case 'needs_review':
                        case 'draft':
                            $scope.draftNode = true;
                            loadFromRevision = true;
                    }
            }
        }
        if (typeof $scope.node[0].moderation_tabs !== "undefined" && $scope.node[0].moderation_tabs !== null) {
            $scope.trustedModerationTabs = $sce.trustAsHtml($scope.node[0].moderation_tabs.toString());
            $scope.trustedModerationBlock = $sce.trustAsHtml($scope.node[0].moderation_block.toString());
        }

        // Revisions are not in HalJSON format.  Need to check where to get the data from.
        if (!loadFromRevision) {
            $scope.issues = data._embedded['hal:publication_issues'];
            $scope.relatedArticles = data._embedded['hal:publication_related_articles'];
            $scope.nestedArticles = data._embedded['hal:publication_nested_articles'];
            $scope.themes = data._embedded['hal:publication_themes_no_metadata'];
            $scope.subNavigationArticles= data._embedded['hal:publication_sub-navigation_articles'];
            $scope.previousArticles= data._embedded['hal:publication_previous_articles'];
            $scope.nextArticles= data._embedded['hal:publication_next_articles'];
           
        }
        else {
            if (typeof $scope.node[0].nested_articles !== "undefined" && $scope.node[0].nested_articles !== null) {
                $scope.nestedArticles = $scope.node[0].nested_articles;
            }
            if (typeof $scope.node[0].related_articles !== "undefined" && $scope.node[0].related_articles !== null) {
                $scope.relatedArticles = $scope.node[0].related_articles;
            }
            if (typeof $scope.node[0].themes !== "undefined" && $scope.node[0].themes !== null) {
                $scope.themes = $scope.node[0].themes;
                 
            }
            if (typeof $scope.node[0].subNavigationArticle !== "undefined" && $scope.node[0].subNavigationArticle !== null) {
                $scope.subNavigationArticle = $scope.node[0].subNavigationArticle;
            }
            if (typeof $scope.node[0].previousArticles !== "undefined" && $scope.node[0].previousArticles !== null) {
                $scope.previousArticles = $scope.node[0].previousArticles;
            }
            if (typeof $scope.node[0].nextArticles !== "undefined" && $scope.node[0].nextArticles !== null) {
                $scope.nextArticles = $scope.node[0].nextArticles;
            }
            if (typeof $scope.node[0].issues !== "undefined" && $scope.node[0].issues !== null) {
                $scope.issues = $scope.node[0].issues;
            }
        }
        if (typeof $scope.node[0].contents.articleBody !== "undefined" && $scope.node[0].contents.articleBody !== null) {
            $scope.trustedContentBody = $sce.trustAsHtml($scope.node[0].contents.articleBody.toString());
        }
        if (Array.isArray($scope.nestedArticles)){
           index = 0;
           $scope.nestedArticles.forEach(function (nestedArticle){
              $scope.nestedArticles[index].sub_headline = $filter('boldFilter')($scope.nestedArticles[index].sub_headline);
              $scope.nestedArticles[index].headline = $filter('boldFilter')($scope.nestedArticles[index].headline); 
              $scope.nestedArticles[index].trustedNestedContentBody = $sce.trustAsHtml(nestedArticle.articleBody.toString());
               index++;
          });
        }
        if (Array.isArray($scope.relatedArticles)){
           index = 0;      
             $scope.relatedArticles.forEach(function (relatedArticles){
             if ($scope.relatedArticles[index].sub_headline){
                 $scope.relatedArticles[index].sub_headline = $filter('boldFilter')($scope.relatedArticles[index].sub_headline);
              }
              $scope.relatedArticles[index].headline = $filter('boldFilter')($scope.relatedArticles[index].headline); 
             index++;
          });
        }
        if (Array.isArray($scope.subNavigationArticles)){
              index = 0;   
              $scope.subNavigationArticles.forEach(function (subNavigationArticles){
                $scope.subNavigationArticles[index].sub_headline = $filter('boldFilter')($scope.subNavigationArticles[index].sub_headline);
                $scope.subNavigationArticles[index].headline = $filter('boldFilter')($scope.subNavigationArticles[index].headline); 
                $scope.subNavigationArticles[index].aliasCheck = $filter('matchLast')($scope.subNavigationArticles[index].alias);
                if(Array.isArray($scope.subNavigationArticles[index].aliasCheck)){
                  $scope.subNavigationArticles[index].aliasCheck = ($scope.subNavigationArticles[index].aliasCheck.toString());
                }
             index++;
          });
        }
        if (Array.isArray($scope.previousArticles)){
           index = 0;      
            $scope.previousArticles.forEach(function (previousArticles){
            $scope.previousArticles[index].sub_headline = $filter('boldFilter')($scope.previousArticles[index].sub_headline);
            $scope.previousArticles[index].headline = $filter('boldFilter')($scope.previousArticles[index].headline);
            $scope.previousArticles[index].issue  = $filter('boldFilter')($scope.previousArticles[index].issue);   

              if($scope.previousArticles[index].category === 'Feature'){
                 return ;
              }
              if($scope.previousArticles[index].category.length >= 2){
                 $scope.previousArticles[index].category =  $scope.previousArticles[index].category[1];
              }
             index++;
          });
        }
        if (Array.isArray($scope.nextArticles)){
            index = 0;  
            $scope.nextArticles.forEach(function (nextArticles){
              $scope.nextArticles[index].sub_headline = $filter('boldFilter')($scope.nextArticles[index].sub_headline);
              $scope.nextArticles[index].headline = $filter('boldFilter')($scope.nextArticles[index].headline); 
              if($scope.nextArticles[index].category === 'Feature'){
                   return ;
                }
              if($scope.nextArticles[index].category.length >= 2){
                 $scope.nextArticles[index].category =  $scope.nextArticles[index].category[1];
              }
            index++;
          });
        }
        $scope.node[0].metatags['title'] = $filter('removeFilter')($scope.node[0].metatags['title']); 

        $scope.dataHasLoaded = true;
        uwmParallaxOff();
        function checkWidth() {
          var windowSize = $(window).width();
          if (windowSize > 768) {
            uwmParallaxOn();
          }
          else{
            uwmParallaxOff();
          }
        }
        checkWidth();
        setWindowHeight();

        $(window).resize(checkWidth);
          if ($scope.dataHasLoaded){
            $('.loader').css({'display' : '','background' :''});

            // highlightedFactsCarouselSingle();
            highlightedFactsCarousel();
          } 
    });
}]);
magApp.controller('ItemController',['$filter','$scope', '$sce', function($filter,$scope, $sce) {
    $scope.trustedContentBody = $sce.trustAsHtml($scope.node[0].body.replace(/\*(.+?)\*/g, '<span>$1</span>' ).toString());
}]);
magApp.controller('WebPagesController',['$scope',  '$stateParams', 'dftServiceMetaData', 'dftServiceWebPage', '$sce', 'apiPath', function($scope,  $stateParams, dftServiceMetaData, dftServiceWebPage, $sce, apiPath) {
    path = $stateParams.path;
    loadFromRevision = false;
    $scope.dataHasLoaded = false;
    dftServiceWebPage.getNode(path, apiPath, function(data) {
        $scope.node = data['hal:publication_web_pages'];
        dftServiceMetaData.setMetaTags($scope.node[0].metatags);
        // Load the draft version if unpublished or viewing a draft revision.
        if (typeof $scope.node[0].current_status !== 'undefined') {
            $currentStatus = $scope.node[0].current_status;
            switch ($currentStatus) {
                case 'unpublished':
                    $scope.draftNode = true;
                case 'published':
            }
            if (path.endsWith('/draft') && Array.isArray($scope.node[0].moderation_revision) && $scope.node[0].moderation_revision[0] != null) {
                // Set the node to the revision.
                $scope.node = $scope.node[0].moderation_revision;

                $revisionState = $scope.node[0].revision_state;
                switch ($revisionState) {
                    case 'archive':
                    case 'needs_review':
                    case 'draft':
                        $scope.draftNode = true;
                        loadFromRevision = true;
                }
            }
            
     
        }
        $scope.trustedContentBody = $sce.trustAsHtml($scope.node[0].body.toString());
            $scope.dataHasLoaded = true;

        if (typeof $scope.node[0].moderation_tabs !== "undefined" && $scope.node[0].moderation_tabs !== null) {
            $scope.trustedModerationTabs = $sce.trustAsHtml($scope.node[0].moderation_tabs.toString());
            $scope.trustedModerationBlock = $sce.trustAsHtml($scope.node[0].moderation_block.toString());
            $scope.dataHasLoaded = true;
        
        }
        if ($scope.dataHasLoaded){
          highlightedFactsCarousel();
        } 
      });     
}]);
magApp.controller('TopMenuController',['$filter','$log','$scope','$state' ,'$stateParams','$sce','apiPath', 'dftService1',function($filter,$log,$scope,$state,$stateParams, $sce, apiPath, dftService1) {
    dftService1.getMenu(apiPath, function(data) {
       $scope.topMenu = data['hal:publication_main_menu'];
        if (Array.isArray($scope.topMenu)){
          index = 0;
          $scope.topMenu.forEach(function (){
          $scope.topMenu[index].aliasCheck = $filter('matchLast')($scope.topMenu[index].alias);
            if(!$scope.topMenu[index].aliasCheck){
              $scope.topMenu[index].aliasCheck ='home';
            }
            if(Array.isArray($scope.topMenu[index].aliasCheck)){
     
                 $scope.topMenu[index].aliasCheck = ($scope.topMenu[index].aliasCheck.toString());
            }
         index++;
        
      });
    }

    });
}]);
magApp.controller('FooterMenuController',['$filter','$scope','$state' ,'$stateParams', 'apiPath', 'dftService8', function($filter,$scope,$state ,$stateParams,apiPath, dftService8) {
    dftService8.getMenu(apiPath, function(data) {
        $scope.footerMenu = data['hal:publication_footer_menu'];

      });
}]);
magApp.controller('NodeController',['$filter','$scope', '$attrs', 'apiPath', 'dftService2', function($filter,$scope, $attrs, apiPath, dftService2) {
    category = $attrs.category;
    if ( ! $attrs.range ) {
        range = 0
    }
    else {
        range = $attrs.range;
    }
    dftService2.getNode(category, range, apiPath, function(data) {
        $scope.nodes = data['hal:publication_articles'];
    });
}]);
magApp.controller('GalleryController',['$filter','$scope', '$attrs', 'apiPath', 'dftService3', function($filter,$scope, $attrs, apiPath, dftService3) {
    nid = $attrs.node;
    dftService3.getNode(nid, apiPath, function(data) {
        $scope.images = data['hal:publication_image_gallery'][0]['images'];
    });
}]);
magApp.controller('EventController',['$filter','$scope', '$attrs', 'apiPath', 'dftService4', function($filter,$scope, $attrs, apiPath, dftService4) {
    range = $attrs.range;
    dftService4.getNode(range, apiPath, function(data) {
        $scope.events = data['hal:publication_events'];
    });
}]);
magApp.controller('PromoItemsController', ['$filter','$scope', '$location', 'apiPath', 'dftServicePromoItems', '$sce', function($filter,$scope, $location, apiPath, dftServicePromoItems, $sce) {
    currentPath = $location.path();
    dftServicePromoItems.getPromoItems(apiPath, currentPath, function(data) {
      $scope.items = data['hal:publication_promo_items'];
       if (Array.isArray($scope.items)){
            index = 0;
            $scope.items.forEach(function (item){
             $scope.items[index].heading =  $filter('boldFilter')($scope.items[index].heading);
             $scope.items[index].background_image =  $scope.items[index].background_image;
             $scope.items[index].trustedContent =  $filter('boldFilter')($sce.trustAsHtml(item.content.toString()));
              index++;
          });
        }
    });
}]);
magApp.controller('TestimoniesPromoController', ['$filter','$scope', '$location','apiPath', 'dftServicePromoItems', '$sce', function( $filter, $scope, $location, apiPath, dftServicePromoItems, $sce) {
  currentPath = $location.path();
  dftServicePromoItems.getPromoItems(apiPath, currentPath, function(data) {
    $scope.items = data['hal:publication_promo_items'];          
      if (Array.isArray($scope.items)){
        index = 0;
        $scope.items.forEach(function (item){
          $scope.items[index].heading =  $filter('boldFilter')($scope.items[index].heading);
          $scope.items[index].background_image =  $scope.items[index].background_image;
          $scope.items[index].trustedContent =  $filter('boldFilter')($sce.trustAsHtml(item.content.toString()));
          index++;
        });
      }       
    });
}]);
magApp.controller('TestimoniesController', ['$filter','$scope', 'apiPath','dftServiceMetaData', 'dftServiceTestimonies', '$sce', function( $filter, $scope,  apiPath, dftServiceMetaData, dftServiceTestimonies, $sce) {
  dftServiceMetaData.setMetaDataAPI(apiPath);
  $scope.metaData = dftServiceMetaData.getMetaData;
  dftServiceTestimonies.getTestimonies(apiPath, function(data) {
    $scope.items = data['hal:publication_promo_items'];
    dftServiceMetaData.setMetaTags($scope.items[0].metatags);
      if (Array.isArray($scope.items)){
          index = 0;
        $scope.items.forEach(function (item){
          $scope.items[index].heading =  $filter('boldFilter')($scope.items[index].heading);
          $scope.items[index].trustedContent =  $filter('boldFilter')($sce.trustAsHtml(item.content.toString()));
          index++;
        });
      }       
    });
}]);
// The service grabs the data from the json file.
magApp.factory('dftService1',['$http', 'apiPath', function($http) {
    return {
        getMenu: function(apiPath, callback) {
            $http.get( apiPath + '/v1.0/publication_main_menu').success(callback);
        }
    };
}]);
//The service grabs the data from the json file.
magApp.factory('dftService2',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function(category, range, apiPath, callback) {
            $http.get( apiPath + '/v1.0/publication_articles?filter[categories]=' + category + '&&range=' + range).success(callback);
        }
    };
}]);
//The service grabs the data from the json file.
magApp.factory('dftService3',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function(nid, apiPath, callback) {
            $http.get( apiPath + '/v1.0/publication_image_gallery/' + nid).success(callback);
        }
    };
}]);
//The service grabs the data from the json file.
magApp.factory('dftService4',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function(range, apiPath, callback) {
            $http.get( apiPath + '/v1.0/publication_events?range=' + range).success(callback);
        }
    };
}]);
//The service grabs the data from the json file.
magApp.factory('dftServicePromoItems',['$http', 'apiPath', function($http, apiPath) {
    return {
        getPromoItems: function(apiPath, checkPath, callback) {
            filter = "";
            if (checkPath) {
                // Remove any '/' from beginning of the path.
                unwantedCharacter = '/';
                while (checkPath.charAt(0) == unwantedCharacter) checkPath = checkPath.substr(1);
                filter = '?visibility=' + checkPath;
            }
            $http.get( apiPath + '/v1.0/publication_promo_items' + filter).success(callback);
        }
    };
}]);
magApp.factory('dftServicePublicationIssues',['$http', 'apiPath', function($http, apiPath) {
    return {
        getIssues: function(apiPath, callback, issue) {
            filter = "";
            if (issue) {
               filter = '?filter[name]=' + issue;
            }
            $http.get(apiPath + '/v1.0/publication_issues' + filter).success(callback);
        }
    };
}]);
//The service grabs the data from the json file.
magApp.factory('dftServicePublicationArticles',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function(issueName, categoryName, nodeName, apiPath, callback) {
            // Path is like /node/#
            //if(issueName == 'node' && !isNaN(categoryName)) {
            //    $http.get( apiPath + '/v1.0/publication_articles/' + categoryName).success(callback);
            //}
            //else {
                $http.get(apiPath + '/v1.0/publication_articles?filter[alias]=' + issueName + '/' + categoryName + '/' + nodeName).success(callback);
            //}
        }
    };
}]);
magApp.factory('dftServiceTerm',['$http', 'apiPath', function($http, apiPath) {
    return {
        getTerm: function(resource, machineName , apiPath, callback) {
            $http.get( apiPath + '/v1.0/' + resource + '?filter[machine_name]='  + machineName ).success(callback);
        }
    };
}]);
magApp.factory('dftServiceTermNode',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function(filter, termName , apiPath, callback) {
            $http.get( apiPath + '/v1.0/publication_articles_teaser?filter['+ filter +']='  + termName ).success(callback);
        }
    };
}]);
magApp.factory('dftServiceTermMetaData',['$http', 'apiPath', function($http, apiPath) {
    return {
        getTerm: function(resource, id , apiPath, callback) {
            $http.get( apiPath + '/v1.0/'+ resource + '/' + id).success(callback);
        }
    };
}]);
magApp.factory('dftService8',['$http', 'apiPath', function($http, apiPath) {
    return {
        getMenu: function(apiPath, callback) {
            $http.get(apiPath + '/v1.0/publication_footer_menu').success(callback);
        }
    };
}]);
magApp.factory('dftServiceWebPage',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function( path, apiPath, callback) {
           $http.get( apiPath + '/v1.0/publication_web_pages?filter[alias]=' + path).success(callback);
        }
    };
}]);
magApp.factory('dftServiceDetermineBundle',['$http', 'apiPath', function($http, apiPath) {
    return {
        getNode: function( nodeID, apiPath, callback) {
            $http.get( apiPath + '/v1.0/determine_bundle/' + nodeID).success(callback);
        }
    };
}]);
magApp.factory('dftServicePACurrentIssue', ['$http', function($http) {
    return {
      getNodes: function(apiPath, callback) {

        $http.get( apiPath + '/v1.0/publication_articles_teaser?filter[in_current_issue]=true').success(callback);

      }
    };
}]);
magApp.factory('dftServiceMetaData', ['$http', 'apiPath', function($http, apiPath) {
    var factory = {};
    factory.metaData = {};
    factory.setMetaDataAPI = function(apiPath) {
       $http.get( apiPath + '/v1.0/publication_meta_data').success(function(data) {
         factory.metaData = data['hal:publication_meta_data'];
       });
    };
    factory.setMetaTags = function(metaTags) {
        factory.metaData.metatags = metaTags;
    };
     factory.getMetaData = function() {
       return factory.metaData;
    };
    return factory;
}]);
