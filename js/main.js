var $window = $(window);
var $windowHeight = $(window).innerHeight();
 var windowSize = $(window).width();
var headerHeight = 80;
var footerHeight = 240;
var headerHeightDesktop = 72; 
 //header height = number before change of positioning css
var menuHeight = 50;
var heroHeightCLass = 'uwm-hero';
var $parallaxId = '.uwm-article-cover-parallax';//class for setting hero image height of view 
var setHeight = 'uwm-main-content';//class for setting min-height to main wrap element
function is_ie() {
    // Disable for IE
    var ua = window.navigator.userAgent;
    var old_ie = ua.indexOf('MSIE ');
    var new_ie = ua.indexOf('Trident/');

    if ((old_ie > -1) || (new_ie > -1)) {
        return true;
    }
}
//---------------------------------------------
/* Dynamic top menu positioning
 *
 */
     $(window).bind('scroll', function () {
         var windowSize = $(window).width();
         if (windowSize <= 1024){ 
            if ($('body').hasClass('publication')){
            var num = 52;

          }
          else  if ($('body').hasClass('magazine')){
            var num = 52;
           
          }
         else  if ($('body').hasClass('sotu3')){
            var num = 56;

            }
          else{
            var num = 80; 
           }

          if($('.uwm-header-bar').hasClass('bt-menu-close')){
            if ($(window).scrollTop() > num) {
                $('.uwm-header-wrapper').addClass('fixed-to-top');
                $('.uwm-logo-header').addClass('hide-logo');
                $('.uwm-article-cover-parallax').addClass('header-fixed-to-top');
                $('.uwm-hero-feature').addClass('header-fixed-to-top');
                $('.uwm-main-content').addClass('header-fixed-to-top');
            } else {
                $('.uwm-header-wrapper').removeClass('fixed-to-top');
                $('.uwm-logo-header').removeClass('hide-logo');
                $('.uwm-article-cover-parallax').removeClass('header-fixed-to-top');
                $('.uwm-hero-feature').removeClass('header-fixed-to-top');
                $('.uwm-main-content').removeClass('header-fixed-to-top');
            }
          }
          else if($('.uwm-header-bar').hasClass('bt-menu-open')){
             $('.uwm-logo-header').removeClass('hide-logo');
             $('.uwm-header-wrapper').removeClass('fixed-to-top');
             $('.uwm-article-cover-parallax').removeClass('header-fixed-to-top');
             $('.uwm-hero-feature').removeClass('header-fixed-to-top');
             $('.uwm-main-content').removeClass('header-fixed-to-top');
          }
        }
        else{
          if ($('body').hasClass('publication')){
            $('.uwm-header-wrapper').addClass('fixed-to-top');
            $('.uwm-logo-header').removeClass('hide-logo');
            $('.uwm-article-cover-parallax').addClass('header-fixed-to-top');
            $('.uwm-hero-feature').addClass('header-fixed-to-top');
            $('.uwm-main-content').addClass('header-fixed-to-top');
          }
          else if ($('body').hasClass('magazine')){
             $('.uwm-header-wrapper').addClass('fixed-to-top');
             $('.uwm-logo-header').removeClass('hide-logo');
             $('.uwm-article-cover-parallax').addClass('header-fixed-to-top');
             $('.uwm-hero-feature').addClass('header-fixed-to-top');
             $('.uwm-main-content').addClass('header-fixed-to-top');
          }
        }
    
  });
function uwmScrollTop(){
    var stop = $('html').offset().top - 80;
    var delay = 300;
    $('body,html').animate({scrollTop: stop}, delay);
    return false;
}
function uwmScrollTopSOTU(){
    var stop = $('html').offset().top - 80;
    var delay = 300;
    $('.uwm-right').animate({scrollTop: stop}, delay);
    return false;
}
function uwmMenuTop(){
    var stop = $('.uwm-header-bar-wrapper').offset().top;
    if ($('.uwm-header-bar-wrapper').hasClass('menuAbsolute')){
      // var stop = $('.uwm-header-bar-wrapper').offset().top -headerHeight;
    }
    var delay = 0;
    $('body,html').animate({scrollTop: stop}, delay);
    return false;
}
//---------------------------------------------
function setWindowHeight() {
    $('.uwm-main-content-wrapper').css('min-height', $(window).innerHeight());
    $('.uwm-web-page-body').css('min-height', $(window).innerHeight() - footerHeight *2);
    $('.uwm-right').css('min-height', $(window).innerHeight() - footerHeight *2);
    $('.uwm-pub-cat').css('min-height', $(window).innerHeight() - footerHeight *2);
    $('.uwm-feature-hp-block').css('min-height', $(window).innerHeight() - footerHeight - headerHeight - 103 + 'px');
  }
function highlightedFactsCarousel($delay) {
  var delay = $delay;
  setTimeout(function(){
     var highlightedFact = $('div[class*="highlighted-fact-wrapper"]');
      highlightedFact.owlCarousel({
      items : 3,
      itemsDesktop : [1024,3],
      itemsTablet : [768,2],
      itemsMobile : [480,1],
      navigation : true,
      navigationText : ["&lsaquo;&nbsp;prev","next&nbsp;&rsaquo;"]
   }); 
  }, delay);   
 
}   
function highlightedFactsCarouselSingle($delay) {
  var delay = $delay;
  setTimeout(function(){
     var highlightedFact = $("[data-highlighted-carousel].single");
      highlightedFact.owlCarousel({
      items : 1,
      itemsDesktop : [1024,1],
      itemsTablet : [768,1],
      itemsMobile : [480,1],
      navigation : true,
      navigationText : ["&lsaquo;&nbsp;prev","next&nbsp;&rsaquo;"]
   }); 
  }, delay);   

}  
//---------------------------------------------



function uwmParallaxOn(){
  
      $($parallaxId).removeClass('P-ON');
    $('body').removeClass('no-parallax-bg');
    $(".is-parallax").each(function(){
        var $bgobj = $(this); // assigning the object
        var yPos = -( ($window.scrollTop()-$bgobj.offset().top) / 5);
        var coords = 'background-position: center '+ yPos + 'px !important; transition: none;';
        $($parallaxId).addClass('P-ON');
        $bgobj.attr('style', coords);

             $(window).scroll(function() {
              var yPos = -( ($window.scrollTop()-$bgobj.offset().top) / 5);
              // Put together our final background position
              var coords = 'background-position: center '+ yPos + 'px !important; transition: none;';
              // Move the background
              if ($($parallaxId).hasClass('P-ON')) {
                 $bgobj.attr('style', coords);
              }

            });
          
      });
    }
  function uwmParallaxOff(){
    $($parallaxId).removeClass('P-ON');
     $('body').addClass('no-parallax-bg');
     $($parallaxId).css({'background-position' : '','transition' :''});
  }

(function($)
{
    $.fn.removeStyle = function(style)
    {
        var search = new RegExp(style + '[^;]+;?', 'g');

        return this.each(function()
        {
            $(this).attr('style', function(i, style)
            {
                return style.replace(search, '');
            });
        });
    };

}(jQuery));
/*=================================
=            DOM READY            =
=================================*/
;(function uwmInit($, window, document) {
  'use strict';
  $(document).ready(handleDocumentReady);
  function handleDocumentReady() {
    $(document).foundation();
      setWindowHeight(); 
  }   

})(window.jQuery, window, window.document);

/*===================================
=            WINDOW LOAD            =
===================================*/
$(window).load(function() {
  var windowSize = $(window).width();

  // Run clicks on the search button
  $('.search-button').on('click', function(e) {
    e.preventDefault();
    if ($(this).attr('aria-expanded') === 'false') {
      $('.search-button').attr('aria-expanded', 'true');
    }
    else {
      $('.search-button').attr('aria-expanded', 'false');
    }
  });

  var openMenu = function() {
    uwmParallaxOff();
    $('.uwm-top-nav-wrapper').attr({
      'data-nav-visible': 'true'
    });
    $('[href="#navigation"]').attr({
      'aria-expanded': 'true'
    });
    $('.uwm-header-bar').removeClass('bt-menu-close');
    $('.uwm-header-bar').addClass('bt-menu-open');
    $('body').addClass('menu-open');
    $('.uwm-left').addClass('menu-open');
    uwmMenuTop();
  };

  // Basic close menu function
  var closeMenu = function() {
    var windowSize = $(window).width();
    $($parallaxId).removeClass('P-ON');
    if (windowSize > 768){
        uwmParallaxOn();
    }
    $('.uwm-top-nav-wrapper').attr({
      'data-nav-visible': 'false'
    });
    $('[href="#navigation"]').attr({
      'aria-expanded': 'false'
    });
    $('.uwm-header-bar').removeClass('bt-menu-open');
    $('.uwm-header-bar').addClass('bt-menu-close');
    $('body').removeClass('menu-open');
    $('.uwm-left').removeClass('menu-open');
  };

  // Run clicks on the navigation button
  $('[href="#navigation"]').on('click', function(e) {
    e.preventDefault();
    $(this).attr('aria-expanded') === 'true' ? closeMenu() : openMenu();
  });

  // At end of navigation block, return focus to navigation menu button
  $('#navigation li:last-child a').on('keydown', function(e) {
    if (e.keyCode === 9) {
      if (!e.shiftKey) {
        e.preventDefault();
        $('[href="#navigation"]').focus();
      }
    }
  });

    // At start of navigation block, refocus close button on SHIFT+TAB

    $('#navigation li:first-child a').on('keydown', function(e) {
      if (e.keyCode === 9) {
        if (e.shiftKey) {
          e.preventDefault();
          $('[href="#navigation"]').focus();
        }
      }
    });

    $('#navigation li a').on('click',function() {
        closeMenu();
        uwmScrollTop();

    });
    // If the menu is visible, always TAB into it from the menu button

    $('[aria-expanded]').on('keydown', function(e) {
      if (e.keyCode === 9) {
        if ($(this).attr('aria-expanded') == 'true') {
          if (!e.shiftKey) {
            e.preventDefault();
            $('#navigation li:first-child a').focus();
          } else {
            if (e.shiftKey) {
              e.preventDefault();
              $('.uwm-main-content').focus();
            }
          }
        }
      }
    });

  // fire when initial window loads
  setWindowHeight();
});